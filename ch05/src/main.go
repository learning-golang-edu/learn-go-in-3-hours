package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

func main() {
	// Methods
	methods()
	// Interfaces
	interfaces()
	// Errors
	errorsHandling()
}

type Foo struct {
	A int
	B string
}

func (f Foo) String() string {
	return fmt.Sprintf("A: %d and B: %s", f.A, f.B)
}

func (f *Foo) Double() {
	f.A = f.A * 2
}

func (f Foo) fieldCount() int {
	return 2
}

func (f Foo) String2() string {
	return fmt.Sprintf("%d fields: A: %d and B: %s", f.fieldCount(), f.A, f.B)
}

type Bar struct {
	C bool
	Foo
}

func (b Bar) String() string {
	return fmt.Sprintf("%s and C: %v", b.Foo.String(), b.C)
}

func (b Bar) fieldCount() int {
	return 3
}

type myInt int

func (mi myInt) isEven() bool {
	return mi%2 == 0
}

func (mi *myInt) Double() {
	*mi = *mi * 2
}

func methods() {
	f := Foo{
		A: 10,
		B: "Hello",
	}
	fmt.Println(f.String())
	f.Double()
	fmt.Println(f.String())

	fmt.Println("==========")
	fmt.Println()

	f = Foo{
		A: 10,
		B: "Hello",
	}
	b := Bar{
		C:   true,
		Foo: f,
	}
	fmt.Println(b.String())
	b.Double()
	fmt.Println(b.String())

	fmt.Println("==========")
	fmt.Println()

	m := myInt(10)
	fmt.Println(m)
	fmt.Println(m.isEven())
	m.Double()
	fmt.Println(m)
	fmt.Println("==========")
	fmt.Println()
}

type tester interface {
	test(int) bool
}

func runTests(i int, tests []tester) bool {
	result := true
	for _, test := range tests {
		result = result && test.test(i)
	}
	return result
}

type rangeTest struct {
	min int
	max int
}

func (rt rangeTest) test(i int) bool {
	return rt.min <= i && i <= rt.max
}

type divTest int

func (dt divTest) test(i int) bool {
	return i%int(dt) == 0
}

func doStuff(i interface{}) {
	switch i := i.(type) {
	case int:
		fmt.Println("Double i is", i+i)
	case string:
		fmt.Println("i is", len(i), "characters long")
	default:
		fmt.Println("I don't know what to do with this")
	}
}

type testerFunc func(int) bool

func (tf testerFunc) test(i int) bool {
	return tf(i)
}

func interfaces() {
	result := runTests(10, []tester{
		rangeTest{min: 5, max: 20},
		divTest(5),
	})
	fmt.Println(result)

	fmt.Println("==========")
	fmt.Println()

	var i interface{}
	i = "Hello"
	j := i.(string)
	k, ok := i.(int)
	fmt.Println(j, k, ok)
	/*
		m := i.(int) // this line cause panic, because it is invalid conversion
		fmt.Println(m)
	*/

	fmt.Println("==========")
	fmt.Println()

	doStuff(10)
	doStuff("Hello")
	doStuff(true)

	fmt.Println("==========")
	fmt.Println()

	result = runTests(10, []tester{
		testerFunc(func(i int) bool {
			return i%2 == 0
		}),
		testerFunc(func(i int) bool {
			return i < 20
		}),
	})
	fmt.Println(result)

	fmt.Println("==========")
	fmt.Println()
}

func divAndMod(a int, b int) (int, int, error) {
	if b == 0 {
		return 0, 0, errors.New("cannot divide by zero")
	}
	return a / b, a % b, nil
}

type MyError struct {
	A       int
	B       int
	Message string
}

func (me *MyError) Error() string {
	return fmt.Sprintf("values %d and %d produced error %s", me.A, me.B, me.Message)
}

func divAndMod2(a int, b int) (int, int, error) {
	if b == 0 {
		return 0, 0, &MyError{A: a, B: b, Message: "Cannot divide by zero"}
	}
	return a / b, a % b, nil
}

func reallyNil() error {
	var e error
	fmt.Println("e is nil:", e == nil)
	return e
}

func notReallyNil() error {
	var me *MyError
	fmt.Println("me is nil:", me == nil)
	return me
}

func errorsHandling() {
	if len(os.Args) < 3 {
		fmt.Println("Expected two input parameters")
		os.Exit(1)
	}
	a, err := strconv.ParseInt(os.Args[1], 10, 64)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	b, err := strconv.ParseInt(os.Args[2], 10, 64)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	div, mod, err := divAndMod(int(a), int(b))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("%d / %d == %d and %d %% %d == %d\n", a, b, div, a, b, mod)

	fmt.Println("==========")
	fmt.Println()

	if len(os.Args) < 3 {
		fmt.Println("Expected two input parameters")
		os.Exit(1)
	}
	a, err = strconv.ParseInt(os.Args[1], 10, 64)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	b, err = strconv.ParseInt(os.Args[2], 10, 64)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	div, mod, err = divAndMod2(int(a), int(b))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("%d / %d == %d and %d %% %d == %d\n", a, b, div, a, b, mod)

	fmt.Println("==========")
	fmt.Println()

	e := reallyNil()
	me := notReallyNil()
	fmt.Println("in main, e is nil:", e == nil)
	fmt.Println("in main, me is nil:", me == nil)

	fmt.Println("==========")
	fmt.Println()
}
