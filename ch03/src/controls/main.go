package main

import (
	"fmt"
	"os"
)

func main() {
	// if..else statements
	ifElse()
	// for statements
	loops()
	// switch statements
	switches()
	// intro into functions
	functions()
	// advanced functions
	advFunctions()
	// pointers
	pointers()
}

func ifElse() {
	a := 10
	if a > 5 {
		b := a / 2
		fmt.Println("a is bigger than 5:", a, b)
	} else {
		fmt.Println("a is less or equal to 5")
	}

	if b := a / 2; b > 5 {
		fmt.Println("b is bigger than 5:", a, b)
	} else {
		fmt.Println("b is less or equal to 5:", a, b)
	}
}

func loops() {
	a := 3
	fmt.Println("=======")
	for i := 0; i < 10; i++ {
		if i == a {
			continue
		}
		if i > 7 {
			break
		}
		fmt.Println(i)
	}

	fmt.Println("=======")
	j := 0
	for j < 10 {
		fmt.Println(j)
		j = j + 1
	}
	fmt.Println(j)

	fmt.Println("=======")
	j = 0
	for {
		fmt.Println(j)
		j = j + 1
		if j > 10 {
			break
		}
	}
	fmt.Println(j)

	fmt.Println("=======")
	s := "Hello, World!"
	for k, v := range s {
		fmt.Println(k, v, string(v))
	}

	fmt.Println("=======")
	s = "👋 🌍"
	for k, v := range s {
		fmt.Println(k, v, string(v))
	}
}

func switches() {
	word := os.Args[1]
	if word == "hello" {
		fmt.Println("Hi yourself")
	} else if word == "goodbye" {
		fmt.Println("So long!")
	} else if word == "greetings" {
		fmt.Println("Salutations!")
	} else {
		fmt.Println("I don't know what you said")
	}

	greet := "greetings"
	switch l := len(word); word {
	case "hi":
		fmt.Println("Very informal!")
		fallthrough
	case "hello":
		fmt.Println("Hi yourself")
	case "farewell":
	case "goodbye", "bye":
		fmt.Println("So long!")
	case greet:
		fmt.Println("Salutations!")
	default:
		fmt.Println("I don't know what you said but it was", l, "characters long")
	}

	c := "crackerjack"
	switch l := len(word); {
	case word == "hi":
		fmt.Println("Very informal!")
		fallthrough
	case word == "hello":
		fmt.Println("Hi yourself")
	case l == 1:
		fmt.Println("I don't know any one letter word!")
	case 1 < l && l < 10, word == c:
		fmt.Println("The word is either", c, "or it is 2-9 characters long")
	default:
		fmt.Println("I don't know what you said but it was", l, "characters long")
	}
}

func functions() {
	addNumbers()
	addNumbers()

	addNumbers2(2, 3)
	addNumbers2(4, 10)
	addNumbers2(100, -100)

	a := addNumbers3(10, 5)
	fmt.Println(a)

	d, r := divAndRemainder(2, 3)
	fmt.Println(d, r)
	d, _ = divAndRemainder(10, 4)
	fmt.Println(d)
	_, r = divAndRemainder(100, -100)
	fmt.Println(r)
	divAndRemainder(-1, 20)

	a = 1
	arr := [2]int{2, 4}
	s := "hello"
	doubleFail(a, arr, s)
	fmt.Println("in main", a, arr, s)
}

func addNumbers() {
	fmt.Println(2 + 3)
}

func addNumbers2(a int, b int) {
	fmt.Println(a + b)
}

func addNumbers3(a int, b int) int {
	return a + b
}

func divAndRemainder(a int, b int) (int, int) {
	return a / b, a % b
}

func doubleFail(a int, arr [2]int, s string) {
	a = a * 2
	for i := 0; i < len(arr); i++ {
		arr[i] = arr[i] * 2
	}
	s = s + s
	fmt.Println("in doubleFail", a, arr, s)
}

func advFunctions() {
	myAddOne := addOne
	fmt.Println(addOne(1))
	fmt.Println(myAddOne(1))

	myOtherAddOne := func(a int) int {
		return a + 1
	}
	fmt.Println(myOtherAddOne(1))

	printOperation(1, addOne)
	printOperation(1, addTwo)

	b := 2
	myAddOne2 := func(a int) int {
		return a + b
	}
	fmt.Println(myAddOne2(1))

	myAddOne3 := func(a int) {
		b = a + b
	}
	myAddOne3(1)
	fmt.Println(b)

	addOneF := makeAdder(1)
	addTwoF := makeAdder(2)
	fmt.Println(addOneF(1))
	fmt.Println(addTwoF(1))

	doubleAddOne := makeDoubler(addOneF)
	doubleAddTwo := makeDoubler(addTwoF)
	fmt.Println(doubleAddOne(1))
	fmt.Println(doubleAddTwo(1))

}

func addOne(a int) int {
	return a + 1
}

func addTwo(a int) int {
	return a + 2
}

func printOperation(a int, f func(int) int) {
	fmt.Println(f(a))
}

func makeAdder(c int) func(int) int {
	return func(a int) int {
		return c + a
	}
}

func makeDoubler(f func(int) int) func(int) int {
	return func(a int) int {
		d := f(a)
		return d * 2
	}
}

func pointers() {
	fmt.Println("====")
	fmt.Println("")

	a := 10
	b := &a
	c := a
	fmt.Println(a, b, *b, c)

	a = 20
	fmt.Println(a, b, *b, c)

	*b = 30
	fmt.Println(a, b, *b, c)

	c = 40
	fmt.Println(a, b, *b, c)

	var d *int
	fmt.Println(d)
	//	fmt.Println(*d) // this line causes panic

	e := new(int)
	fmt.Println(e)
	fmt.Println(*e)

	f := 20
	fmt.Println(f)
	setToTen(&f)
	fmt.Println(f)

	f = 20
	fmt.Println(f)
	setToTenFail(&f)
	fmt.Println(f)
}

func setToTen(a *int) {
	*a = 10
}

func setToTenFail(a *int) {
	a = new(int)
	*a = 10
}
