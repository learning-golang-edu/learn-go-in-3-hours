package main

import (
	"fmt"
	"net/http"
)

func main() {
	numberTypes()
	stringTypes()
	arrays()
	http.HandleFunc("/hello", func(rw http.ResponseWriter, req *http.Request) {
		name := req.URL.Query().Get("name")
		rw.Write([]byte(fmt.Sprintf("Hello, %s", name)))
	})
	http.ListenAndServe(":8080", nil)
}

func numberTypes() {
	var i int8 = 20
	var j float32 = 5.6
	fmt.Println(float32(i) + j)
	fmt.Println(i + int8(j))
	fmt.Println(i + int8(j+1.9))
	var k int32 = 40
	fmt.Println(int32(i) + k)
}

func stringTypes() {
	var s string
	s = "Hello, World!"
	fmt.Println(s)

	s1 := "Hello, \n\t\"world!\" with backslashes \\"
	fmt.Println(s1)

	s2 := `Hello,
	"world" with backticks \`
	fmt.Println(s2)

	em := "👋 🌍"
	s3 := s + " " + em
	fmt.Println(s3)

	b := s[0]
	b1 := s[4]
	fmt.Println(s, b, string(b), b1, string(b1))

	s4 := s[0:5]
	s5 := s[7:12]
	s6 := s[:5]
	s7 := s[7:]
	fmt.Println(s, s4, s5, s6, s7)

	fmt.Println(len(s), len(s4), len(s5))

	fmt.Println(em, len(em), em[:1], len(em[:1]), em[2:], len(em[2:]))

	h := "Hello, "
	var r rune = 127757
	fmt.Println(h + string(r))

	r1 := '🌍'
	fmt.Println(h + string(r1))
}

func arrays() {
	var vals [3]int
	vals[0] = 2
	vals[2] = 4
	vals[1] = 6
	fmt.Println(vals, vals[0], vals[1], vals[2])
}
