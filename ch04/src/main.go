package main

import (
	"encoding/json"
	"fmt"
	"mapper"
	"math"
	"math/rand"
	"strings"
	"time"
	"unicode/utf8"
)

func main() {
	// packages
	packages()
	// third party packages
	thirdPartyPackages()
	// Slices
	slices()
	// Maps
	maps()
	// Structs
	structs()
}

func packages() {
	s := "This is a test 123"
	s2 := strings.Map(rot13, s)
	fmt.Println(s2)
	s3 := strings.Map(rot13, s2)
	fmt.Println(s3)

	w := "👋 🌍"
	fmt.Println(w)
	fmt.Println(len(w))
	fmt.Println(utf8.RuneCountInString(w))

	rand.Seed(time.Now().UnixNano())
	a := rand.Intn(10)
	b := rand.Intn(10)
	c := int(math.Max(float64(a), float64(b)))
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c, "is bigger")
}

func rot13(in rune) rune {
	if in >= 'A' && in <= 'Z' {
		return (((in - 'A') + 13) % 26) + 'A'
	}
	if in >= 'a' && in <= 'z' {
		return (((in - 'a') + 13) % 26) + 'a'
	}
	return in
}

func thirdPartyPackages() {
	fmt.Println(mapper.Greet("Howdy, what's new?"))
	fmt.Println(mapper.Greet("Comment allez vous?"))
	fmt.Println(mapper.Greet("Wie geht es Ihnen?"))
	fmt.Println(mapper.Greet("Mitä kuuluu?"))
	fmt.Println(mapper.Greet("Как дела?"))
}

func slices() {
	s := make([]string, 0)
	fmt.Println("length of s:", len(s))
	s = append(s, "hello")
	fmt.Println("length of s:", len(s))
	fmt.Println("contents of s[0]:", s[0])
	s[0] = "goodbye"
	fmt.Println("contents of s[0]:", s[0])

	s2 := make([]string, 2)
	fmt.Println("contents of s2[0]:", s2[0])
	s2 = append(s2, "hello")
	fmt.Println("contents of s2[0]:", s2[0])
	fmt.Println("contents of s2[2]:", s2[2])
	fmt.Println("length of s2:", len(s2))

	s3 := []string{"a", "b", "c"}

	for k, v := range s3 {
		fmt.Println(k, v)
	}

	s4 := s3[0:2]
	fmt.Println("s4:", s4)
	s3[0] = "d"
	fmt.Println("s4:", s4)

	var s5 []string
	s5 = s3
	s5[1] = "camel"
	fmt.Println("s3:", s3)

	modSlice(s3)
	fmt.Println("s3[0]:", s3[0])

	uniHello := "👋 🌍"
	bytes := []byte(uniHello)
	fmt.Println(bytes)
	runes := []rune(uniHello)
	fmt.Println(runes)
	runes[1] = 'a'
	fmt.Println(runes)
	fmt.Println(uniHello)
}

func modSlice(s []string) {
	s[0] = "hello"
}

func maps() {
	m := make(map[string]int)
	m["hello"] = 300
	h := m["hello"]
	fmt.Println("hello in m:", h)
	fmt.Println("a in m:", m["a"])

	if v, ok := m["hello"]; ok {
		fmt.Println("hello in m:", v)
	}

	m["hello"] = 20
	fmt.Println("hello in m:", m["hello"])

	m2 := map[string]int{
		"a": 1,
		"b": 2,
		"c": 50,
	}

	for k, v := range m2 {
		fmt.Println(k, v)
	}

	fmt.Println("b in m2:", m2["b"])
	delete(m2, "b")
	fmt.Println("b in m2:", m2["b"])

	mm := map[string]int{
		"a": 1,
		"b": 2,
	}
	var m3 map[string]int

	fmt.Println("goodbye in m:", mm["goodbye"])
	m3 = mm
	m3["goodbye"] = 400
	fmt.Println("goodbye in m3:", m3["goodbye"])
	fmt.Println("goodbye in m:", mm["goodbye"])

	modMap(mm)
	fmt.Println("cheese in m:", mm["cheese"])
}

func modMap(m map[string]int) {
	m["cheese"] = 20
}

type Foo struct {
	A int
	b string
}

type Bar struct {
	C string
	F Foo
}

type Baz struct {
	D string
	Foo
}

type Person struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func structs() {
	f := Foo{}
	fmt.Println(f)

	g := Foo{10, "Hello"}
	fmt.Println(g)

	h := Foo{
		b: "Goodbye",
	}
	fmt.Println(h)

	h.A = 1000
	fmt.Println(h.A)

	f = Foo{
		A: 20,
	}
	var f2 Foo
	f2 = f
	f2.A = 100
	fmt.Println(f2.A)
	fmt.Println(f.A)

	var f3 = &f
	f3.A = 200
	fmt.Println(f3.A)
	fmt.Println(f.A)

	ff := Foo{A: 10, b: "Hello"}
	b1 := Bar{C: "Fred", F: ff}
	fmt.Println(b1.F.A)
	b2 := Baz{D: "Nancy", Foo: ff}
	fmt.Println(b2.A)

	var ff2 = b2.Foo
	fmt.Println(ff2)

	bob := `{ "name": "Bob", "age": 30}`
	var b Person
	json.Unmarshal([]byte(bob), &b)
	fmt.Println(b)
	bob2, _ := json.Marshal(b)
	fmt.Println(string(bob2))
}
