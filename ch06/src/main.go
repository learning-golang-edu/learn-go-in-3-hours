package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	// CSP and Goroutines
	goroutines()
	// Channels
	gochannels()
	// Select
	goselects()
}

func runMe() {
	fmt.Println("Hello from a goroutine")
}

func runMeWithName(name string) {
	fmt.Println("Hello to", name, "from a goroutine")
}

func goroutines() {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		runMe()
		wg.Done()
	}()
	wg.Wait()

	wg.Add(1)
	go func(name string) {
		runMeWithName(name)
		wg.Done()
	}("Edu")
	wg.Wait()

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			fmt.Println(i)
			wg.Done()
		}()
	}
	wg.Wait()

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(localI int) {
			fmt.Println(localI)
			wg.Done()
		}(i)
	}
	wg.Wait()
}

func gochannels() {
	in := make(chan string)
	out := make(chan string)
	go func() {
		name := <-in
		out <- fmt.Sprintf("Hello, " + name)
	}()
	in <- "Edu"
	close(in)
	message := <-out
	fmt.Println(message)

	out1 := make(chan int, 10)
	for i := 0; i < 10; i++ {
		go func(localI int) {
			out1 <- localI * 2
		}(i)
	}
	var result []int
	for i := 0; i < 10; i++ {
		val := <-out1
		result = append(result, val)
	}
	fmt.Println(result)

	in2 := make(chan int)
	out2 := make(chan int)

	go func() {
		for {
			i := <-in2
			out2 <- i * 2
		}
	}()

	in2 <- 1
	o1 := <-out2
	in2 <- 2
	o2 := <-out2
	fmt.Println(o1, o2)

	in3 := make(chan int, 10)
	out3 := make(chan int)

	for i := 0; i < 10; i++ {
		in3 <- i
	}
	close(in3)

	go func() {
		for {
			i, ok := <-in3
			if !ok {
				close(out3)
				break
			}
			out3 <- i * 2
		}
	}()

	for v := range out3 {
		fmt.Println(v)
	}
}

func select1() {
	in := make(chan int)
	out := make(chan int, 1)

	out <- 1

	select {
	case in <- 2:
		fmt.Println("wrote 2 to in")
	case v := <-out:
		fmt.Println("read", v, "from out")
	}
}

func select2() {
	in := make(chan int, 1)
	out := make(chan int, 1)

	out <- 1

	select {
	case in <- 2:
		fmt.Println("wrote 2 to in")
	case v := <-out:
		fmt.Println("read", v, "from out")
	}
}

func select3() {
	in := make(chan int)
	out := make(chan int)

	select {
	case in <- 2:
		fmt.Println("wrote 2 to in")
	case v := <-out:
		fmt.Println("read", v, "from out")
	default:
		fmt.Println("nothing else works")
	}
}

func multiples(i int) chan int {
	out := make(chan int)
	curVal := 0
	// this will run forever and keep all open channels active until program exits
	go func() {
		for {
			out <- curVal * i
			curVal++
		}
	}()
	return out
}

func select4() {
	twosCh := multiples(2)
	for v := range twosCh {
		if v > 20 {
			break
		}
		fmt.Println(v)
	}
}

func multiples2(i int) (chan int, chan struct{}) {
	out := make(chan int)
	done := make(chan struct{})
	curVal := 0
	// this solves previous problem with channels hanging open
	go func() {
		for {
			select {
			case out <- curVal * i:
				curVal++
			case <-done:
				fmt.Println("goroutine shutting down")
				return
			}
		}
	}()
	return out, done
}

func select5() {
	twosCh, done := multiples2(2)
	for v := range twosCh {
		if v > 20 {
			break
		}
		fmt.Println(v)
	}
	close(done)

	//do more stuff
	time.Sleep(1 * time.Second)
}

func select6() {
	in := make(chan int)
	in2 := make(chan int)

	var wg sync.WaitGroup
	wg.Add(3)

	go func() {
		for i := 0; i < 10; i++ {
			in <- i
		}
		close(in)
		wg.Done()
	}()

	go func() {
		for i := 100; i < 110; i++ {
			in2 <- i
		}
		close(in2)
		wg.Done()
	}()

	go func() {
		count := 0
		for count < 2 {
			select {
			case i, ok := <-in:
				if !ok {
					count++
					in = nil
					continue
				}
				fmt.Println("from in, result is", i*2)
			case i, ok := <-in2:
				if !ok {
					count++
					in2 = nil
					continue
				}
				fmt.Println("from in2, result is", i+2)
			}
		}
		wg.Done()
	}()

	wg.Wait()
}

func select7() {
	var wg sync.WaitGroup
	totalStart := time.Now()
	for i := 0; i < 100000; i++ {
		start := time.Now()
		wg.Add(1)
		go func(in int) {
			time.Sleep(1 * time.Second)
			out := 2 * in
			_ = out
			fmt.Println("got", out, "for", in, "after", time.Now().Sub(start))
			wg.Done()
		}(i)
	}
	wg.Wait()
	fmt.Println("total time:", time.Now().Sub(totalStart))
}

func select8() {
	// back pressure example to solve the problem in previous example
	workers := 10000
	pool := make(chan func(int) int, workers)
	for i := 0; i < workers; i++ {
		pool <- func(in int) int {
			time.Sleep(1 * time.Second)
			result := 2 * in
			return result
		}
	}

	var wg sync.WaitGroup
	count := 0
	totalStart := time.Now()
	for i := 0; i < 100000; i++ {
		start := time.Now()
		select {
		case f := <-pool:
			fmt.Println("processing", i)
			count++
			wg.Add(1)
			go func(in int) {
				out := f(in)
				fmt.Println("got", out, "for", in, "after", time.Now().Sub(start))
				pool <- f
				wg.Done()
			}(i)
		default:
			fmt.Println("rejecting request", i, "too busy")
		}
	}
	wg.Wait()
	fmt.Println("total processed:", count)
	fmt.Println("total time:", time.Now().Sub(totalStart))
}

func goselects() {
	/* the following code is producing deadlock and program panics
	in := make(chan int)
	out := make(chan int, 1)

	out <- 1

	in <- 2
	fmt.Println("wrote 2 to in")
	v := <-out
	fmt.Println("read", v, "from out")
	*/

	select1()
	select2()
	select3()
	select4()
	select5()
	select6()
	select7()
	select8()
}
